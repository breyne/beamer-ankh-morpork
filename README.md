A main theme **AnkhMorpork** for beamer-no-brainer commands:

```
\usetheme{AnkhMorpork}
```

A colortheme **WorldBearingTurtle**:

```
\usecolortheme[
 %octarine=MetroDTeal,
 %structure=MetroDTeal,
 %alerted=MetroBrown,
 %example=MetroGreen,
 %%sober
 %ntnu,%dracula,%metropolis,%
]{WorldBearingTurtle}
```

# Ankh-Morpork

*The commands I would always use.*

### Provides

- [x] environments `standout` and `STANDOUT` for standout plain frames.
- [x] `\myBeamerClock{<radius>}{<baselineshift>}`: a display of page numbers.

### Theme contents

Inner theme

- [x] Fonts: Fira Sans + Fira mono for textnormal and texttt, Palatino-like for math
- [x] No leftmargin in itemize
- [x] Footline: simple

Outer theme

- [x] Simple title page with blocks and no centering
- [x] Section pages: plain with beamer-clock

Other

- [x] Load `appendixnumberbeamer` to fix 
- [x] Fix `\inst` and the institute spacing

# World Bearing Turtle

### Theme 

- [x] The theme does not define new colors unless options are specified.
- [x] Instead it sets `color palettes` to redefine everything.
      From there it is easy to customize.


### Provides

- [x] options for preset themes `ntnu`, `dracula`, `metropolis`.
- [x] key=value options `octarine`, `structure`, `alerted`, `example` to assign colors explicitely.
- [x] preset mode `sober` (define last) to make to make title-like clocks standout less.
